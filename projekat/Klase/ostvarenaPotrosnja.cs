﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekat
{
    public class ostvarenaPotrosnja
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        /*
        int satPotrosnje = 0;
        double iznosPotrosnje = 0;
        string sifraGeografskeOblasti = null;
        DateTime vremeUvozaFajla = DateTime.Now;
        string imeFajla = "prog_2018_05_07.xml";
        string lokacija = System.IO.Path.GetDirectoryName(imeFajla);*/

        public int SatPotrosnje { get; set; }
        public double IznosPotrosnje { get; set; }
        public string SifraGeografskeOblasti { get; set; }
        public string VremeUvozaFajla { get; set; }
        public string ImeFajla { get; set; }
        public string Lokacija { get; set; }
    }
}
