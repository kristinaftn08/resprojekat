﻿using projekat.Interfejsi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekat.Klase
{
    public class DodajPodrucje : IDodajPodrucje
    {
        public static List<string> podrucje = new List<string>();

        public void dodajPodrucje(string sifra)
        {
            string path = @"C:\Users\Kristina\Desktop\14.08. verzija KAJA\RESProjeka\projekatKK\projekat\bin\Debug\geografska_podrucja.csv";

            var csvFileLenth = new System.IO.FileInfo(path).Length;

            if (csvFileLenth == 0)
            {
                podrucje.Add(sifra);
                var csv = new StringBuilder();
                var newLine = string.Format("{0},{1}", sifra, sifra);
                csv.AppendLine(newLine);

                File.AppendAllText(path, csv.ToString());

            }
            else
            {
                string[] readText = File.ReadAllLines(path);
                int br = readText.Count();
                bool isti = false;

                for (int i = 0; i < br; i++)
                {

                    if (readText[i] == string.Format("{0},{1}", sifra, sifra))
                    {


                        isti = true;
                        break;

                    }
                }
                if (isti == false)
                {
                    var csv = new StringBuilder();
                    string newLine = string.Format("{0},{1}", sifra, sifra);

                    csv.AppendLine(newLine);
                    podrucje.Add(sifra);

                    File.AppendAllText(path, csv.ToString());
                }
            }
        }
    }
}
