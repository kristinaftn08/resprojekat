﻿using projekat.Interfejsi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekat.Klase
{
    public class ValidacijaFajla : IValidacijaFajla
    {
        public DataTable dataTable = new DataTable();
       public double procenti = 0;

        public string mesec(string pun_naziv_fajla)
        {
            string[] putanja2 = pun_naziv_fajla.Split('.');
            string bez_xml = putanja2[0];
            string[] potrosnja_god_mesec_dan = putanja2[0].Split('_');
            string mesec = potrosnja_god_mesec_dan[2];
            return mesec;

        }

        public string prog_ili_ostv(string pun_naziv_fajla)
        {
            string[] putanja2 = pun_naziv_fajla.Split('.');
            string bez_xml = putanja2[0];
            string[] potrosnja_god_mesec_dan = putanja2[0].Split('_');
            string potrosnja = potrosnja_god_mesec_dan[0];
            return potrosnja;
        }

        public string pun_naziv_fajla(string path)
        {
            string[] delovi_putanje = path.Split('\\');
            string pun_naziv_fajla = delovi_putanje[delovi_putanje.Count() - 1];
            return pun_naziv_fajla;
        }

        public bool broj_linija(int broj_stavki, string mesec, string pun_naziv_fajla)
        {
            bool rez = false;

            if ((broj_stavki) != 48)
            {
                if ((broj_stavki - 1) == 46)
                {
                    if (mesec == "03")
                    {
                        //datum iz fajla
                        string[] putanja2 = pun_naziv_fajla.Split('.');
                        string[] datum_iz_fajla = putanja2[0].Split('_');
                        string datum = string.Format("{0}-{1}-{2}", datum_iz_fajla[1], datum_iz_fajla[2], datum_iz_fajla[3]);
                        DateTime konacan_datum = DateTime.Parse(datum);

                        //pronalaznje poslednje nedelje u martu
                        DateTime nadjen_datum = konacan_datum;
                        nadjen_datum = nadjen_datum.AddMonths(1).AddDays(-1);

                        while (nadjen_datum.DayOfWeek != DayOfWeek.Sunday)
                            nadjen_datum = nadjen_datum.AddDays(-1);
                        if (konacan_datum != nadjen_datum)
                        {
                            rez = false;
                            //MessageBox.Show("NEVALIDAN FAJL");

                        }
                        else
                        {
                            rez = true;
                        }

                    }
                    else
                    {
                        rez = false;
                    }

                }
                else if ((broj_stavki - 1) == 50)
                {
                    if (mesec == "10")
                    {
                        //datum iz fajla
                        string[] putanja2 = pun_naziv_fajla.Split('.');
                        string[] datum_iz_fajla = putanja2[0].Split('_');
                        string datum = string.Format("{0}-{1}-{2}", datum_iz_fajla[1], datum_iz_fajla[2], datum_iz_fajla[3]);
                        DateTime konacan_datum = DateTime.Parse(datum);

                        //pronalaznje poslednje nedelje u martu
                        DateTime nadjen_datum = konacan_datum;
                        nadjen_datum = nadjen_datum.AddMonths(1).AddDays(-1);

                        while (nadjen_datum.DayOfWeek != DayOfWeek.Sunday)
                            nadjen_datum = nadjen_datum.AddDays(-1);
                        if (konacan_datum != nadjen_datum)
                        {
                            rez = false;
                        }
                        else
                        {
                            rez = true;
                        }

                    }
                    else
                    {
                        rez = false;
                    }

                }
                else
                {
                    rez = false;
                }
            }
            else
            {
                rez = true;
            }
            return rez;
        }

        public void odbacivanjeFajla(string pun_naziv_fajla, int broj_stavki)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Vreme citanja");
            dataTable.Columns.Add("Ime fajla");
            dataTable.Columns.Add("Broj redova");
            dataTable.Columns.Add("Lokacija");

            DateTime time = DateTime.Now;
            string ime = pun_naziv_fajla;
            string l = System.IO.Directory.GetCurrentDirectory();       //lokacija
            dataTable.Rows.Add(time, ime, broj_stavki, l);


            // DA BI IMALA OVO KLIKNI DESNI JA PROJEKAT PA IDES MENAGE NUGET PACKAGES
            // INSTALIRAS DocumentFormat.OpenXml 
            // INSTALIRAS ClosedXML I AKO TI I  DALJE CRVENI DODAJ REFERENCU ClosedXML

            ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
            wbook.Worksheets.Add(dataTable, "tabela_odbacenih_fajlova");

            wbook.SaveAs("tabela_odbacenih.xlsx");
        }
        public double racunanje_aritmeticke_sredine() {

            double aritmetika = 0;
            SqlConnection konekcija = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");
            SqlConnection konekcija2 = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");
            konekcija2.Open();
            konekcija.Open();


            SqlCommand komanda = new SqlCommand("select satPotrosnje from dbo.tabelaOstvarena", konekcija);
            SqlCommand komanda2 = new SqlCommand("select satPotrosnje from dbo.tabelaPrognozirana", konekcija2);


            SqlDataReader reader = komanda.ExecuteReader();

            SqlDataReader reader2 = komanda2.ExecuteReader();


            
            int brojac = 0;
            string pomO;
            string pomP;
            double satO = 0;
            double satP = 0;
            while (reader.Read())
            {
                while (reader2.Read())
                {


                    pomO = reader["satPotrosnje"].ToString();
                    satO += Double.Parse(pomO);
                    pomP = reader2["satPotrosnje"].ToString();
                    satP += double.Parse(pomP);
                   
                    brojac++;
                }
            }
            aritmetika = (satO + satP) / brojac;



            return aritmetika;
        }
        public void izracunaj_relativno_odstupanje(DateTime datum, string s)
        {
            DateTime d = new DateTime();
            d = datum;
            string parsiranje = s;
            double sabiranjeP = 0;
            //  string[] ss = parsiranje2[1].Split(' ');
            string sss = parsiranje;
            SqlConnection konekcija = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");
            SqlConnection konekcija2 = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");
            konekcija2.Open();
            konekcija.Open();

            dataTable.Columns.Add("Ostvarena potrosnja");
            dataTable.Columns.Add("Prognozirana potrosnja");
            dataTable.Columns.Add("Relativno odstupanje");


            SqlCommand komanda = new SqlCommand("select iznosPotrosnje from dbo.tabelaOstvarena where sifraGeografskeOblasti like @s", konekcija);
            SqlCommand komanda2 = new SqlCommand("select iznosPotrosnje from dbo.tabelaPrognozirana  where sifraGeografskeOblasti like @s", konekcija2);

            komanda.Parameters.AddWithValue("@s", sss);

            komanda2.Parameters.AddWithValue("@s", sss);


            SqlDataReader reader = komanda.ExecuteReader();

            SqlDataReader reader2 = komanda2.ExecuteReader();


            double rel_odstupanje = 0;
            int brojac = 0;
            double aritmetika = racunanje_aritmeticke_sredine();
            
            while (reader.Read() && reader2.Read())
            {
               

                    string pom;
                    string pom2;
                    double o = 0;
                    double p = 0;
   

                    pom = reader["iznosPotrosnje"].ToString();
                    o = Double.Parse(pom);
                    pom2 = reader2["iznosPotrosnje"].ToString();
                    p = double.Parse(pom2);
                    double oduzimanje = o - p;
                    double deljenje = (double)(oduzimanje / o);
                    double finalno = deljenje * 100;
                    rel_odstupanje = finalno;
                    sabiranjeP += rel_odstupanje;
                    brojac++;
                    dataTable.Rows.Add(reader["iznosPotrosnje"], reader2["iznosPotrosnje"], rel_odstupanje);
                    
                
            }
            procenti = sabiranjeP / brojac;

            // DA BI IMALA OVO KLIKNI DESNI JA PROJEKAT PA IDES MENAGE NUGET PACKAGES
            // INSTALIRAS DocumentFormat.OpenXml 
            // INSTALIRAS ClosedXML I AKO TI I  DALJE CRVENI DODAJ REFERENCU ClosedXML
            
            ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
            wbook.Worksheets.Add(dataTable, "tabela_relativnog_odstupanja");
            wbook.SaveAs("tabela_relativnog_odstupanja.xlsx");
        }

    }
}
