﻿using projekat.Interfejsi;
using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace projekat.Klase
{
    public class Tabela : ITabela
    {
        int broj_stavki;
        private DataSet ds;
        private XmlReader xmlFile;
        private SQLiteConnection connection;
        private IDodajPodrucje novo_podrucje = new DodajPodrucje();
        string vreme_uvoza;
        string lokacija;

        public IDodajPodrucje Novo_podrucje
        {
            get
            {
                return novo_podrucje;
            }

            set
            {
                novo_podrucje = value;
            }
        }

        public int kreiraj_tabelu(string path, string cela_putanja, string vrsta_potrosnje)
        {
            if (vrsta_potrosnje == "prog")
            {
                DateTime vremeUvozaFajla = DateTime.Now;
                lokacija = System.IO.Path.GetDirectoryName(path);
                string format = "yyyy-MM-dd HH:mm:ss";
                vreme_uvoza = vremeUvozaFajla.ToString(format);

                //string connetionString = null;
                //SqlDataAdapter adpter = new SqlDataAdapter();
                ds = new DataSet();


                string databaseName = "Prognozirana_potrosnja.db";

                //connetionString = System.IO.Path.Combine(cela_putanja, databaseName);
                 //connection = new SQLiteConnection(connetionString);

                xmlFile = XmlReader.Create(cela_putanja, new XmlReaderSettings());
                ds.ReadXml(xmlFile);
                broj_stavki = ds.Tables[0].Rows.Count;  //DA LI JE BROJ LINIJA OVO ILI TREBA -1
                return broj_stavki;
            }
            else if (vrsta_potrosnje == "ostv")
            {

                DateTime vremeUvozaFajla = DateTime.Now;
                lokacija = System.IO.Path.GetDirectoryName(path);
                string format = "yyyy-MM-dd HH:mm:ss";
                vreme_uvoza = vremeUvozaFajla.ToString(format);

                //string connetionString = null;
                //SqlDataAdapter adpter = new SqlDataAdapter();
                ds = new DataSet();


                string databaseName = "Ostvarena_potrosnja.db";

                //connetionString = System.IO.Path.Combine(cela_putanja, databaseName);
                // connection = new SQLiteConnection(connetionString);

                xmlFile = XmlReader.Create(cela_putanja, new XmlReaderSettings());
                ds.ReadXml(xmlFile);
                broj_stavki = ds.Tables[0].Rows.Count;
                return broj_stavki;
            }
            else { }
            return broj_stavki;
        }
        public void upis_u_bazu(string pun_naziv_fajla, string vrsta_tabele)
        {
            int i = 0;
            if (vrsta_tabele == "prog")
            {
                prognoziranaPotrosnja moja_baza = new prognoziranaPotrosnja();  //suntimes 
                moja_baza.ImeFajla = pun_naziv_fajla;
                moja_baza.Lokacija = System.IO.Directory.GetCurrentDirectory();
                moja_baza.VremeUvozaFajla = vreme_uvoza;
                DateTime vreme = DateTime.Now;
                SqlConnection konekcija = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");

                konekcija.Open();
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    moja_baza.SatPotrosnje = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0]);
                    moja_baza.IznosPotrosnje = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[1]);
                    moja_baza.SifraGeografskeOblasti = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    novo_podrucje.dodajPodrucje(ds.Tables[0].Rows[i].ItemArray[2].ToString());

                    SqlCommand komanda = new SqlCommand("INSERT INTO dbo.tabelaPrognozirana(satPotrosnje,iznosPotrosnje,sifraGeografskeOblasti,vremeUvozaFajla,imeFajla,lokacija) VALUES (@satPotrosnje,@iznosPotrosnje,@sifraGeografskeOblasti,@vremeUvozaFajla,@imeFajla,@lokacija)", konekcija);

                    komanda.Parameters.AddWithValue("@satPotrosnje", moja_baza.SatPotrosnje);
                    komanda.Parameters.AddWithValue("@iznosPotrosnje", moja_baza.IznosPotrosnje);
                    komanda.Parameters.AddWithValue("@sifraGeografskeOblasti", moja_baza.SifraGeografskeOblasti);
                    komanda.Parameters.AddWithValue("@vremeUvozaFajla", vreme);
                    komanda.Parameters.AddWithValue("@imeFajla", moja_baza.ImeFajla);
                    komanda.Parameters.AddWithValue("@lokacija", moja_baza.Lokacija);
                    komanda.ExecuteNonQuery();

                }
                konekcija.Close();
            }
            else if (vrsta_tabele == "ostv")
            {

                ostvarenaPotrosnja ostv = new ostvarenaPotrosnja();   //suntimes 

                ostv.ImeFajla = pun_naziv_fajla;
                ostv.Lokacija = Directory.GetCurrentDirectory();
                ostv.VremeUvozaFajla = vreme_uvoza;

                DateTime vreme = DateTime.Now;
                SqlConnection konekcija = new SqlConnection("Server=localhost;Database=prognozirana_Potrosnja;Trusted_Connection=True;");

                konekcija.Open();
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    ostv.SatPotrosnje = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0]);
                    ostv.IznosPotrosnje = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[1]);
                    ostv.SifraGeografskeOblasti = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    novo_podrucje.dodajPodrucje(ds.Tables[0].Rows[i].ItemArray[2].ToString());
                    SqlCommand komanda = new SqlCommand("INSERT INTO dbo.tabelaOstvarena(satPotrosnje,iznosPotrosnje,sifraGeografskeOblasti,vremeUvozaFajla,imeFajla,lokacija) VALUES (@satPotrosnje,@iznosPotrosnje,@sifraGeografskeOblasti,@vremeUvozaFajla,@imeFajla,@lokacija)", konekcija);

                    komanda.Parameters.AddWithValue("@satPotrosnje", ostv.SatPotrosnje);
                    komanda.Parameters.AddWithValue("@iznosPotrosnje", ostv.IznosPotrosnje);
                    komanda.Parameters.AddWithValue("@sifraGeografskeOblasti", ostv.SifraGeografskeOblasti);
                    komanda.Parameters.AddWithValue("@vremeUvozaFajla", vreme);
                    komanda.Parameters.AddWithValue("@imeFajla", ostv.ImeFajla);
                    komanda.Parameters.AddWithValue("@lokacija", ostv.Lokacija);
                    komanda.ExecuteNonQuery();


                }
                konekcija.Close();
            }

        }
    }
}
