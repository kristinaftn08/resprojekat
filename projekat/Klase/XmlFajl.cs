﻿using projekat.Interfejsi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace projekat.Klase
{
    public class XmlFajl : IXmlFajl
    {
        public string proveraXml()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();


            dlg.DefaultExt = ".xml";
            dlg.Filter = "XML Files (*.xml) | *.xml";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {

                return dlg.FileName;

            }
            else
            {
                MessageBox.Show("Uneti fajl mora imati xml ekstenziju.");
                return "";
            }
        }
    }
}
