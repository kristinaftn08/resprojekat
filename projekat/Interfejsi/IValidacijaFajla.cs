﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekat.Interfejsi
{
    public interface IValidacijaFajla
    {
        string pun_naziv_fajla(string path);
        string mesec(string pun_naziv_fajla);
        string prog_ili_ostv(string pun_naziv_fajla);
        bool broj_linija(int broj_stavki, string mesec, string pun_naziv_fajla);
        void odbacivanjeFajla(string pun_naziv_fajla, int broj_stavki);
        void izracunaj_relativno_odstupanje(DateTime datum, string s);
    }
}
