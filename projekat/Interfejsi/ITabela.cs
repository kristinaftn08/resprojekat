﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekat.Interfejsi
{
    public interface ITabela
    {
        int kreiraj_tabelu(string path, string cela_putanja, string vrsta_potrosnje);
        void upis_u_bazu(string pun_naziv_fajla, string vrsta_tabele);
    }
}
