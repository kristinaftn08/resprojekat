﻿using projekat.Interfejsi;
using projekat.Klase;
using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;


namespace projekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public string path;
        ValidacijaFajla validacija = new ValidacijaFajla();
        ITabela tabela = new Tabela();
        IXmlFajl xml = new XmlFajl();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e) //unit??
        {
            
            string rezultatProvere = xml.proveraXml();

            if (rezultatProvere != "")
            {

                path = rezultatProvere;
                label1.Content = path;
                var uri = new Uri(path);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            string naziv_fajla = validacija.pun_naziv_fajla(path);
            string prog_ili_ostv = validacija.prog_ili_ostv(naziv_fajla);
            string mesec = validacija.mesec(naziv_fajla);

            int broj = tabela.kreiraj_tabelu(path, naziv_fajla, prog_ili_ostv);


            if (validacija.broj_linija(broj, mesec, naziv_fajla) == true)
            {
                tabela.upis_u_bazu(naziv_fajla, prog_ili_ostv);
                List<string> podrucje = DodajPodrucje.podrucje;
                for (int i = 0; i < podrucje.Count; i++)
                {
                    sifraGeo.ItemsSource = podrucje;
                }
                MessageBox.Show("Fajl je uspesno upisan u bazu podataka.");

            }
            else
            {
                validacija.odbacivanjeFajla(naziv_fajla, broj);
                MessageBox.Show("Uneli ste nevalidan fajl. Broj linija ne odgovara broju sati u danu.");
            }


        }

        private void izracunaj_dugme(object sender, RoutedEventArgs e)
        {
            
            DateTime vreme = new DateTime();
            vreme = kalendar.SelectedDate.Value;
         
            string sifra = sifraGeo.SelectedItem.ToString();
            validacija.izracunaj_relativno_odstupanje(vreme,sifra);
            grid1.ItemsSource = validacija.dataTable.DefaultView;
            MessageBox.Show("U CSV fajl su uspesno upisano izracunate vrednosti.");
            double pomA = validacija.racunanje_aritmeticke_sredine();
            sredina.Content = pomA.ToString();
            double pomPr = validacija.procenti;
            procentualno.Content = pomPr.ToString();
        }

        private void SifraGeo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}

