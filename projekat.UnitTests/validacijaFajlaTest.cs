﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace projekat.UnitTests
{
    [TestClass]
    public class validacijaFajlaTest
    {

        [TestMethod]
        public void broj_linija__24__returnsFalse()
        {
            var validacija_fajla = new validacija_fajla();

            var result = (validacija_fajla.Broj_stavki == 48 && validacija_fajla.Mesec == "03" && validacija_fajla.Mesec == "10");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void broj_linija__23__returnsFalse()
        {
            var validacija_fajla = new validacija_fajla();

            var result = ((validacija_fajla.Broj_stavki == 46) && (validacija_fajla.Mesec != "05"));

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void broj_linija__25__returnsFalse()
        {
            var validacija_fajla = new validacija_fajla();

            var result = ((validacija_fajla.Broj_stavki == 50) && (validacija_fajla.Mesec != "10"));

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void prog_ili_ostv__prog__returnsTrue()
        {
            var validacija_fajla = new validacija_fajla();
            var result = validacija_fajla.prog_ili_ostv("C:\\Users\\Kristina\\Desktop\\zadatak_2\\RESProjeka\\projekatKK\\projekat\\bin\\Debug\\prog_2018_05_07.xml");

            Assert.AreEqual(result, "prog");
        }

        [TestMethod]
        public void prog_ili_ostv__ostv__returnsTrue()
        {
            var validacija_fajla = new validacija_fajla();
            var result = validacija_fajla.prog_ili_ostv("C:\\Users\\Kristina\\Desktop\\zadatak_2\\RESProjeka\\projekatKK\\projekat\\bin\\Debug\\ostv_2018_05_07.xml");

            Assert.AreEqual(result, "ostv");
        }
        [TestMethod]
        public void prog_ili_ostv__greska__returnsTrue()
        {
            var validacija_fajla = new validacija_fajla();
            var result = validacija_fajla.prog_ili_ostv("C:\\Users\\Marko\\Desktop\\RES projekat\\Projekat03.06.2019_CSV\\projekatKK\\projekat\\bin\\Debug\\prog_2018_05_07.xml");

            Assert.AreNotEqual("", result);
        }


    }
}
